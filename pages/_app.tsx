import '@/styles/globals.css'
import type { AppProps } from 'next/app'

// 新增Provider和Store
import { Provider } from "react-redux";
import store from "../store/index";

export default function App({ Component, pageProps }: AppProps) {
  // 注入store和Provider
  return <Provider store={store}>
  <Component {...pageProps} />
</Provider>
}