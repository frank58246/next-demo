import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '@/store'
import { fetchTodos } from '@/utils/api'

// 定義Counter的state資料
interface CounterState {
  value: number,
  isFetching: boolean
}

// 初始化Counter的state
const initialState: CounterState = {
  value: 0,
  isFetching: false
}

export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  
  // 同步的reducer
  reducers: {
    increment: state => {
      state.value += 1
    },
    decrement: state => {
      state.value -= 1
    },
    
    // 有參數的方法
    incrementByAmount: (state, action: PayloadAction<number>) => {
      state.value += action.payload
    }
  },
  
  // 非同步的reducer
  extraReducers: (builder) => {
    
    // 定義發起非同步方法執行時，要做的事情
    builder.addCase(fetchTodos.pending, (state) => {
      state.isFetching= true;
    });

    // 定義非同步方法回傳時，要做的事情
    builder.addCase(fetchTodos.fulfilled, (state, { payload }) => {
      state.value += payload.length
      state.isFetching= false;
    });
  }
})

// 開放給外部dispatch呼叫的方法
export const { increment, decrement, incrementByAmount } = counterSlice.actions

// 開放給外部取得state的資料
export const selectCount = (state: RootState) => state.counter.value

// 把reducer export出去
export default counterSlice.reducer