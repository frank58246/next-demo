// Counter.tsx
import React from 'react'
import { decrement, increment } from '../reducers/counterSlice'
import { useAppSelector, useAppDispatch } from '../store'
import { fetchTodos } from '@/utils/api'

export function Counter() {

  // 使用定義好的dispatch和selector
  const count = useAppSelector(state => state.counter.value)
  const isFetching = useAppSelector(state => state.counter.isFetching)
  const dispatch = useAppDispatch()

  return (
    <div>
      <div>
        <button
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>

        <button
          aria-label="Increment Async value"
          onClick={() => dispatch(fetchTodos(10))}
        >
          Increment Async value
        </button>
        <span>{count}</span>

        <button
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>


        {isFetching && <p> fetching</p>}

      </div>
    </div>
  )
}