import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../reducers/counterSlice'
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'

// 建立store，底下可以有多個reducer
const store = configureStore({
  reducer: {
    // 新增reducer
    counter: counterReducer,

  }
})

// 定義state的型別
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

// 定義hook
export const useAppDispatch: () => AppDispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

export default store