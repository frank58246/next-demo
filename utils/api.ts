import { createAsyncThunk } from "@reduxjs/toolkit";
type Todo = {
  id: string;
  title: string;
  completed: boolean;
};


// 建立取得Todo的Thunk，參數是number，回傳Todo陣列
export const fetchTodos = createAsyncThunk<Todo[], number>(
    "todos/fetch", 
    async (limit: number) => {
      const response = await fetch(
        // 這邊的limit只是舉例
        // placeholder API 並不會只回傳limit的數量
        
        `https://jsonplaceholder.typicode.com/todos?limit=${limit}`
      );
      
      return await response.json();
    }
  );